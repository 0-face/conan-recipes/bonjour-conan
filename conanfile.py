from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

class Recipe(ConanFile):
    name           = "bonjour"
    description    = "Apple implementation of DNS Service Discovery (libdns_sd)"
    license        = "Apache License v2.0"

    version        = "878.30.4"
    source_sha256  = "83ae1245ce8612438dd358a83c8c4a4f1348725d0dbf0703cf520882589229af"
    settings       = "os", "compiler", "arch", "build_type"

    dir_name       = "mDNSResponder-" + version
    source_url     = "https://opensource.apple.com/tarballs/mDNSResponder"
    homepage       = "https://developer.apple.com/bonjour/"

    url            = "https://gitlab.com/0-face/conan-recipes/bonjour-conan"


    options = {
        'pie': [True, False],
        'extra_cflags': "ANY",
        'use_soname': [True, False]
    }
    default_options = (
        'pie=False',
        'extra_cflags=',
        'use_soname=False'
    )

    def source(self):
        zip_name = self.dir_name + ".tar.gz"
        url = "{}/{}".format(self.source_url, zip_name)

        tools.download(url, zip_name)
        tools.check_sha256(zip_name, self.source_sha256)
        tools.unzip(zip_name)
        os.remove(zip_name)

    def configure(self):
        # package is pure c
        del self.settings.compiler.libcxx

    def build(self):
        if self.is_posix():
            self.patch_sources()
            self.build_posix()
        else:
            raise RuntimeError("The Build for this OS ('%s') is not implemented yet." % self.settings.os)

    def patch_sources(self):
        self.patch_posix_makefile()
        self.patch_clients_makefile()

    def build_posix(self):
        with tools.chdir(self.posix_build_dir):
            self.make_targets("clean")

            # Build first 'libdns_sd' with specific flags
            self.make_targets("setup libdns_sd", self.libdns_sd_flags())

            # Then build the executables, using (potentially) other flags
            self.make_targets("setup Daemon Clients", self.executables_flags())

    def package(self):
        self.copy("dns_sd.h", src=path.join(self.dir_name, "mDNSShared"), dst="include")

        for suffix in ("so", "lib", "dll", "dylib*"):
            self.copy("*.%s" % suffix, dst="lib", keep_path=False)

        self.copy("dns-sd", dst="bin", src=self.clients_out_path)
        self.copy("mdnsd", dst="bin", src=self.posix_out_path)

    def package_info(self):
        self.cpp_info.libs = ['dns_sd']

        self.env_info.PATH.append(path.join(self.package_folder, "bin"))


################################# Patch steps ########################################################

    def patch_posix_makefile(self):
        posix_makefile = path.join(self.source_folder, self.dir_name, 'mDNSPosix', 'Makefile')

        # patch posix makefile to allow add CFLAGS and LDFLAGS
        tools.replace_in_file(
            posix_makefile,
            "CFLAGS = $(CFLAGS_COMMON) $(CFLAGS_OS) $(CFLAGS_DEBUG)",
            '\n'.join([
                "CFLAGS := $(CFLAGS_COMMON) $(CFLAGS_OS) $(CFLAGS_DEBUG) $(CFLAGS)",
                "LINKOPTS += $(LDFLAGS)"
            ]),
            strict=False
        )

        # patch posix makefile to allow set link options specific to 'libdns_sd'
        tools.replace_in_file(
            posix_makefile,
            "$(BUILDDIR)/libdns_sd.$(LDSUFFIX): $(CLIENTLIBOBJS)\n" \
            + "\t" + "@$(LD) $(LINKOPTS) -o $@ $+",
            "$(BUILDDIR)/libdns_sd.$(LDSUFFIX): $(CLIENTLIBOBJS)\n" \
            + "\t" + "@$(LD) $(LINKOPTS) $(LINKOPTS_LIBDNS_SD) -o $@ $+",
            strict=False)

    def patch_clients_makefile(self):
        # patch 'Clients' makefile to use CFLAGS and LDFLAGS
        clients_makefile = path.join(self.source_folder, self.dir_name, 'Clients', 'Makefile')
        tools.replace_in_file(
            clients_makefile,
            "$(CC) $(filter %.c %.o, $+) $(LIBS) -I../mDNSShared -Wall",
            "$(CC) $(filter %.c %.o, $+) $(LIBS) -I../mDNSShared -Wall $(CFLAGS) $(LDFLAGS)",
            strict=False
        )

        tools.replace_in_file(
            clients_makefile,
            "LIBS = -L../mDNSPosix/build/prod/ -ldns_sd",
            "\n".join([
                "ifeq ($(DEBUG),1)",
                "BUILDDIR_NAME=debug",
                "else",
                "BUILDDIR_NAME=prod",
                "endif",
                "",
                "LIBS = -L../mDNSPosix/build/$(BUILDDIR_NAME)/ -ldns_sd",
            ]),
            strict=False
        )



################################# Build helpers ########################################################

    def make_targets(self, targets, env=None):
        env = self.make_env(env)
        with tools.environment_append(env):
            debug_opt = 1 if self.is_debug_build else 0

            self.run_and_log(
                    "make DEBUG=%d os=%s %s" % (debug_opt, self.makefile_os, targets)
                    , env=env)

    def make_env(self, env=None):
        env = env or {}

        autotools_vars = AutoToolsBuildEnvironment(self).vars
        self.output.info("Autotools default vars: {}".format(autotools_vars))

        for flags_name, flags in autotools_vars.items():
            env[flags_name] = env.get(flags_name, '') + ' ' + flags

        return env

    def libdns_sd_flags(self):
        env = self.common_flags()

        if self.options.use_soname == True:
            env['LINKOPTS_LIBDNS_SD'] = " -Wl,-soname=libdns_sd.$(LDSUFFIX)"

        return env

    def executables_flags(self):
        env = self.common_flags()

        if self.options.pie:
            env['LDFLAGS'] = env.get('LDFLAGS', '') + " -pie -rdynamic"

        return env

    def common_flags(self):
        env = {}
        CFLAGS = ''

        if self.options.extra_cflags:
            CFLAGS += str(self.options.extra_cflags)

        if self.options.pie == True:
            CFLAGS += " -fPIC"

        if CFLAGS:
            env['CFLAGS'] = CFLAGS.strip()

        return env

    def run_and_log(self, cmd, cwd=None, env=None):
        msg = ''
        if cwd:
            msg = "cwd='{}'\n".format(cwd)
        if env:
            msg += "env='{}'\n".format(env)

        if cwd or env:
            msg += "\t"

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

######################################## Properties ####################################################

    def is_posix(self):
        return self.makefile_os is not None

    @property
    def makefile_os(self):
        # Maps conan OSes to available OSes in posix makefile
        posix_os = {
            "Macos"     : "x",
            "Linux"     : "linux",
            "Android"   : "linux", # Warning: not tested (may not work)
            "FreeBSD"   : "freebsd",
            "SunOS"     : "solaris"
        }


        return posix_os.get(str(self.settings.os), None)

    @property
    def posix_build_dir(self):
        return path.join(self.build_folder, self.dir_name, 'mDNSPosix')

    @property
    def clients_out_path(self):
        return path.join(self.build_folder, self.dir_name, 'Clients', 'build')

    @property
    def posix_out_path(self):
        build_dir_name = 'debug' if self.is_debug_build else 'prod'

        return path.join(self.posix_build_dir, 'build', build_dir_name)

    @property
    def is_debug_build(self):
        return self.settings.build_type == "Debug"
