# Bonjour - conan recipe

[Conan.io](https://www.conan.io/) recipe for [Apple DNS-SD implementation][bonjour_home].

This recipe builds:

* libdns_sd library - used to interact with the mdnsd daemon
* mdnsd - daemon that handles the dns-sd protocol
* dns-sd - client tool to interact with the mdnsd daemon

[bonjour_home]: https://developer.apple.com/bonjour/

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[bonjour documentation][bonjour_home] to instruction in how to use the library.


## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms (see LICENSE.md).

Below we reproduce the library 'LICENSE' file:

> The majority of the source code in the mDNSResponder project is licensed
> under the terms of the Apache License, Version 2.0, available from:
>    <http://www.apache.org/licenses/LICENSE-2.0>
>
> To accommodate license compatibility with the widest possible range
> of client code licenses, the shared library code, which is linked
> at runtime into the same address space as the client using it, is
> licensed under the terms of the "Three-Clause BSD License".
>
> The Linux Name Service Switch code, contributed by National ICT
> Australia Ltd (NICTA) is licensed under the terms of the NICTA Public
> Software Licence (which is substantially similar to the "Three-Clause
> BSD License", with some additional language pertaining to Australian law).

## Limitations

This package currently does not build for all OSes supported by the library.
Also, not all the build options are exposed.
Please, contribute to improve the package.
