#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
import os
from os import path

class TestRecipe(ConanFile):
    settings = "os", "compiler", "arch"
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.1.1@noface/stable"
    )

    generators = "Waf"
    exports_sources = "./*.cpp", "wscript"

    def imports(self):
        self.copy("*.dll"   , src="bin", dst="bin")
        self.copy("*.dylib*", src="lib", dst="bin")

    def build(self):
        cmd = 'waf -v configure build -o "{}"'.format(path.join(self.build_folder, 'build'))

        self.output.info("Running cmd '{}' in '{}'".format(cmd, self.source_folder))

        self.run("ls", cwd=self.source_folder)
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        from conans import tools

        if tools.cross_building(self.settings):
            self.output.info("Cross building - skip run test")
            return

        example_cmd = './build/example'
        self.output.info("running test: '{}' in '{}'".format(example_cmd, self.build_folder))
        self.run(example_cmd, cwd=self.build_folder)
