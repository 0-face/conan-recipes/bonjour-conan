#!/usr/bin/env python
# -*- coding: utf-8 -*-


from os import path

from conan.packager import ConanMultiPackager

def main():
    builder = ConanMultiPackager(
                    reference=get_package_reference(),
                    build_types=["Release"])

    builder.add_common_builds(pure_c=True)

    add_pie_builds(builder, pie_option_name    ="bonjour:pie")

    builder.run()

#####################################################################

def add_pie_builds(builder, pie_option_name):
    '''Add builds with and without PIE(Position Independent Executable).
    '''

    builds = []

    for settings, options, env_vars, build_requires in builder.builds:

        # without PIE
        options[pie_option_name] = False
        builds.append([settings, options, env_vars, build_requires])

        # with PIE
        copy_options = options.copy()
        copy_options[pie_option_name] = True
        builds.append([settings, copy_options, env_vars, build_requires])

    builder.builds = builds


#####################################################################

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect

    conanfile = import_module(path.abspath('conanfile.py'))
    module_members = inspect.getmembers(conanfile, is_recipe)
    print("module_members({}): {}".format(conanfile, module_members))

    for name, member in module_members:
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def import_module(module_path):
    import sys
    import imp

    # Change module name to avoid conflict with ConanMultiPackager
    name = 'temp_' + path.splitext(path.basename(module_path))[0]

    sys.dont_write_bytecode = True
    loaded = imp.load_source(name, module_path)
    sys.dont_write_bytecode = False

    return loaded

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)


#####################################################################

if __name__ == "__main__":
    main()
