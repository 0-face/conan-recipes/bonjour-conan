#include <dns_sd.h>

#include <iostream>

using namespace std;


int main(){
    std::cout << "*************** Bonjour example ********************" << std::endl;


    uint32_t daemonVersion = 0;
    uint32_t resultSize = sizeof(daemonVersion);

    // Get daemon version if running
    DNSServiceErrorType err = DNSServiceGetProperty(
                                    kDNSServiceProperty_DaemonVersion, &daemonVersion, &resultSize);

    if(err == kDNSServiceErr_NoError){
        cout << "mDNS daemon version is: " << daemonVersion << endl;
    }
    else if(err == kDNSServiceErr_ServiceNotRunning){
        cout << "mDNS daemon is not running!" << endl;
    }
    else{
        cerr << "Unknown error!" << endl;
    }

    std::cout << "****************************************************" << std::endl;

    return 0;
}
